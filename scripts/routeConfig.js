angular.module('angularApp')
.config(function ($routeProvider, $locationProvider){
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider.when("/index.html", {
        templateUrl: "/templates/lobby.html",
        controller  : 'defaultCtrl'
    });
                    
    $routeProvider.when("/game", {
        templateUrl: "/templates/game.html",
        controller  : 'gameCtrl'
    });
                    
    $routeProvider.otherwise({
        templateUrl: "/templates/lobby.html",
        controller  : 'defaultCtrl'
    });
})
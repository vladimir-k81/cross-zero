angular.module('angularApp')
.controller('gameCtrl', function($scope, $interval, storage, drawService, searchWinner, $location){
    var REFRESH_INTERVAL = 1000;

    drawService.initGame();

    $scope.currentGame = drawService.getCurrentGame();
    $scope.currentTime = + new Date();
    $scope.gameDuration=$scope.currentTime - $scope.currentGame.gameBeginTime;
    $scope.owner = $scope.currentGame.owner;
    $scope.opponent = $scope.currentGame.opponent;
    $scope.winner = $scope.currentGame.gameResult;
    $scope.currentTurn = drawService.getCurrentTurn();
    $scope.gameMode=drawService.getGameMode();
    
    if ($scope.gameMode == "viewing" && $scope.currentTurn == 1){
        $("#owner").addClass("underlineView");
    } else if ($scope.gameMode == "viewing" && $scope.currentTurn == 2){
        $("#opponent").addClass("underlineView");
    }
    
    // angular.element(".underline").addClass("underlineView");
    $interval(function(){
        $scope.currentTime = + new Date();
        $scope.gameDuration=$scope.currentTime - $scope.currentGame.gameBeginTime;
        $scope.winner = $scope.currentGame.gameResult;
    },REFRESH_INTERVAL);

    $scope.getWinner = function(){
        $scope.winner = $scope.currentGame.gameResult;
        $scope.gameLastAction = $scope.currentGame.gameLastAction;
        $scope.currentTurn = drawService.getCurrentTurn();
    };
    
    $scope.goToLobby = function(){
        $("#modal").modal('hide');    
        $location.path("/index.html");
        storage.setGameInProcess(false);
    };
    
    $scope.surrender = function(){
        searchWinner.surrender($scope.currentTurn);
        $scope.winner = $scope.currentGame.gameResult;
    };

});
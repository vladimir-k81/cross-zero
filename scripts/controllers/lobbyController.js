angular.module('angularApp')
.controller('defaultCtrl', function ($scope, storage, $location, $interval, $routeParams){

    $scope.games=storage.getGames();
    
    $interval(function(){
            $scope.games=storage.getGames();
    },2000);

    $scope.owner = "Player 1";
    $scope.opponent = "Player 2";
    $scope.currentView = 'gameList';
    $scope.select = {value:3};
    $scope.fieldSize = 3;
    $scope.countForWin = 3;

    $scope.gameTypes = [
        {gameType:'3 x 3', value:3},
        {gameType:'4 x 4, четыре в ряд', value:4},
        {gameType:'5 x 5, четыре в ряд', value:5},
        {gameType:'6 x 6, четыре в ряд', value:6},
        {gameType:'Custom field', value:'custom'},
        ];

    $scope.changeFieldSize = function(size){
        if (size == 'custom'){
            $scope.fieldSize = 6;
            $scope.countForWin = 4;
            
        } else {
            if (size == 3){
                $scope.fieldSize = size;
                $scope.countForWin = size;
            } else {
                $scope.fieldSize = size;
                $scope.countForWin = 4;
            }
        }
    };
    
    $scope.createNewGame = function(){
        var gameToken =
            storage.createNewGame($scope.owner, $scope.opponent, $scope.fieldSize, $scope.countForWin);

        storage.setGameInProcess(true);
        $scope.goToGame(gameToken);
    };
    
    $scope.goToGame = function(gameToken){
        $location.path("/game").search({token: gameToken});
    };
    
    $scope.clickHandler = function(item){
        storage.setGameInProcess(true);
        $scope.goToGame(item.gameToken);
    };
});
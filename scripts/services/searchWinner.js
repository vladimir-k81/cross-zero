angular.module('angularApp')
.factory('searchWinner', function(storage){
    
    var MODAL_ID = 'modal';
    var GAME_STATE_DONE = "done";
    
    var increm = [
        [0, -1],
        [1, -1],
        [1, 0],
        [1, 1],
        [0, 1],
        [-1, 1],
        [-1, 0],
        [-1, -1]
        ];
    
    var currentGame = [];
    var map = [];
    var winner = 0;
    var countForWin = 3;
    
	function searchNeighbor(x, y){
	    for(var i=0; i<increm.length; i++){
		    var yi = y + increm[i][0];
			var xi = x + increm[i][1];
			if (yi >= 0 && xi >=0 && yi <= map.length-1 && xi <= map.length-1){

			    if (map[y][x] == map[yi][xi]){
			    	searchVector(yi, xi, i);
			    }
			} 
	    }
	}
    
    function searchVector(y, x, track){
		var count=2;
	    var yi = y;
	    var xi = x;
	    for (var i = 2; i < countForWin; i++){
	        yi = yi + increm[track][0];
    		xi = xi + increm[track][1];
    		if (yi >= 0 && xi >=0 && yi <= map.length-1 && xi <= map.length-1){
				if (map[y][x] == map[yi][xi]){
					count++;
					if (count == countForWin && map[y][x] != 0){
	    						
						winner = map[y][x];
						winnerProcessing(winner);
					}
				}
			}
	    }
	}
	
	function winnerProcessing(winner){
		if (winner == 2){
		    currentGame.gameResult = currentGame.opponent;
		    currentGame.opponentWin = true;
		    currentGame.state = GAME_STATE_DONE;
			$("#"+MODAL_ID).modal('show');
		} 
	    				
	    if (winner == 1){
	    	currentGame.gameResult = currentGame.owner;
	    	currentGame.ownerWin = true;
	    	currentGame.state = GAME_STATE_DONE;
			$("#"+MODAL_ID).modal('show');
		}
		storage.saveGame(currentGame);
		storage.endGame();
	}
    
    return {
        search: function (game){
			winner = 0;
			currentGame = game;
	        map = currentGame.map;
	        countForWin = currentGame.countForWin;
		    for(var i=0; i<map.length; i++){
		        for(var j=0; j< map.length; j++){
					        	
		        	if (currentGame.map[j][i] == 0){
		        		continue;
		        	}
					        	
			        if (winner != 0){
			          	break;
			        }
			      searchNeighbor(i,j);
		        }
		    }
		    return winner;
		},
		
        findDrawResult: function findDrawResult(currentGame){
	        if (winner==0){
	        	var emptyCells = 0;
	        	for(var i=0; i<map.length; i++){
	    	        for(var j=0; j< map.length; j++){
	    	        	if (map[j][i] == 0){
	    	        		emptyCells++;
	    	        	}
	    	        }
	    	    }
	    		if (emptyCells==0){
	    			currentGame.gameResult = "draw";
	    	    	currentGame.state = GAME_STATE_DONE;
	    	    	storage.saveGame(currentGame);
	    			$("#"+MODAL_ID).modal('show');
	    			storage.endGame();
	    		}
	        }
	    },
        surrender:function surrender(player){
	    	if(player==1){
	    		winnerProcessing(2);
	    	} else  if (player==2){
	    		winnerProcessing(1);
	    	}
	    }
    };
});
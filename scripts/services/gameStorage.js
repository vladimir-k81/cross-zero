angular.module('angularApp')
.factory('storage', function($interval, $localStorage){
    
    var REFRESH_INTERVAL = 2000;
    var MAX_IDLE_TIME = 300000;

    var sync_disable = false;

    var currentTime = + new Date();
    var games = [];
        
    if (angular.isDefined($localStorage.games)) {
        games = $localStorage.games;
    } else {
        $localStorage.games = games;
    }
    
    function localStorageSync(){
        if ($localStorage.games.length != games.length){
            games = $localStorage.games;
        } 
    }

    $interval(function(){
        if (sync_disable != true){
            localStorageSync();
            currentTime = + new Date();
            incremGameDuration();
        }
    },REFRESH_INTERVAL);
    
    function incremGameDuration(){
        for (var i=0; i<$localStorage.games.length; i++){
            if ($localStorage.games.length == games.length){
                $localStorage.games[i].gameDuration = currentTime - $localStorage.games[i].gameBeginTime;
            }
        }
        clearOldGames();
    }
    
    function clearOldGames(){
        for(var i = $localStorage.games.length-1; i > -1; i--){
	        if ($localStorage.gameInProcess == true){
	            if (currentTime - $localStorage.games[i].gameLastAction>MAX_IDLE_TIME){
                    games[i].show = false;
                }
	        } else {
	            if (currentTime - $localStorage.games[i].gameLastAction>MAX_IDLE_TIME){
                    deleteGame(i);
                }
	        }
	    }
    }
    
    function deleteGame(item){
        $localStorage.games.splice(item,1);
    }
    
    
    function createMap(numberOfCells, value){
        var map=[];
			for (var i=0; i<numberOfCells; i++){
				map[i] = [];
				for (var j = 0; j<numberOfCells; j++){
					map[i][j] = value;
				}
			}
		return map;
	}

    function findGame(gameToken){
        var findGame;
        for (var i =0; i<$localStorage.games.length; i++){
                if (gameToken == $localStorage.games[i].gameToken){
                    findGame = i;
                }
            }
        return findGame; 
    }

    function setGameInProcess(sign){
        $localStorage.gameInProcess = sign;
    }

    function syncEnable(){
        sync_disable = false;
    }

    return {
        createNewGame: function (owner, opponent, fieldSize, countForWin){
            var gameToken =  + new Date();
            $localStorage.games.push({
                gameToken: gameToken,
                owner: owner,
                ownerWin:false,
                opponent: opponent,
                opponentWin:false,
                size: fieldSize,
                countForWin: countForWin,
                map: createMap(fieldSize, 0),
                gameBeginTime:currentTime,
                gameLastAction:currentTime,
                gameDuration:0,
                gameResult:"",
                state: "ready",
                acessToken:"768b762c8c28",
                gameLastTurn:2,
                show:true,
            });
            sync_disable = true;
            return gameToken;
        },
        setCurrentTime: function (){
            currentTime = + new Date();
            return currentTime;
        },
        endGame:function (){
            setGameInProcess(false);
            syncEnable();
        },
        getGames: function (){
            var gamesInStorage = $localStorage.games;
            return gamesInStorage;
        },
        getCurrentGame: function (gameToken){
            var gameIndex = findGame(gameToken);
            return $localStorage.games[gameIndex];
        },
        getCurrentGameIndex:  function (gameToken){
            var gameIndex = findGame(gameToken);
            return gameIndex;
        },
        setTurn: function (turn){
            $localStorage.turn = turn;
        },
        setGameInProcess: function (sign){
            $localStorage.gameInProcess = sign;
        },
        saveGame:function (savingGame){
            var index = findGame(savingGame.gameToken);
            $localStorage.games[index] = savingGame;
        }
    };
    
});
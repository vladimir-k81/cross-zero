angular.module('angularApp')
.factory('drawService', function(storage, $interval, $location, searchWinner){

	var gameToken = $location.search().token;
	var currentGame = storage.getCurrentGame(gameToken);
	var gameMode = 'normal';
	    
	var CROSS_COLOR = "rgb(188, 188, 188)";
	var CIRCLE_COLOR = "rgb(43, 168, 218)";
	var GRID_COLOR = "rgb(254, 214, 50)"; 
    
    var GAME_STATE_READY = "ready";
    var GAME_STATE_PLAYING = "playing";
    var GAME_STATE_DONE = "done";
    
    var LINE_WIDTH = 0.05;
	var RIGHT_CELL_MARGIN = 0.8;
	var LEFT_CELL_MARGIN = 0.2;
	var DEFAULT_CELL_RADIUS = 1 / 3;

    var MODAL_ID = 'modal';
    var REFRESH_INTERVAL_IN_VIEW_MODE = 2000;

	var myCanvas = document.getElementById("myCanvas12");
	var context = myCanvas.getContext("2d");
	var numberOfCells = currentGame.size;
	var winner = 0;
	var canvasWidth = myCanvas.width;
	var canvasHeight = myCanvas.height;
	var cellSize = canvasWidth/numberOfCells;
	var lineVidth = cellSize*LINE_WIDTH;    
    context.lineWidth = lineVidth;
    
    var map = currentGame.map;
    
    var oldData = currentGame.gameLastTurn;			
	var data = changeTurn();

    function appendCanvasClick(){
    	
    	myCanvas.onclick = function(event) { 
			if (winner == 0){
				var x = (event.pageX - myCanvas.offsetLeft) / cellSize | 0;
				var y = (event.pageY - myCanvas.offsetTop)  / cellSize | 0;
				currentGame.gameLastAction= storage.setCurrentTime();
				if (map[y][x] == 0){
					map[y][x] = data;
					oldData = data;
					currentGame.gameLastTurn=data;
					currentGame.map = map;
					storage.saveGame(currentGame);
					storage.setGameInProcess(true);
					changeTurn();
				} 
			refresh();
			winner = searchWinner.search(currentGame);
			searchWinner.findDrawResult(currentGame);
			}
		};
    }
    
    function viewing(){
    	var refreshInterval = $interval(function(){
    		currentGame = storage.getCurrentGame(gameToken);
    		map = currentGame.map;
    		refresh();
    		if(currentGame.state == GAME_STATE_DONE){
    			$interval.cancel(refreshInterval);
    		}
    	},REFRESH_INTERVAL_IN_VIEW_MODE);
    }

    function drawGrid(){
        context.strokeStyle = GRID_COLOR;
		for (var i = 1; i < numberOfCells; i++) {
			context.moveTo(cellSize*i, 0);
			context.lineTo(cellSize*i, canvasHeight);
			context.stroke();
		}
		for (var j = 1; j < numberOfCells; j++){
			context.moveTo(0, cellSize*j);
			context.lineTo(canvasWidth, cellSize*j);
			context.stroke();
		}
    }

    function drawСross(x,y){
		context.beginPath();
		context.strokeStyle = CROSS_COLOR;
		context.moveTo(x+cellSize*LEFT_CELL_MARGIN, y+cellSize*LEFT_CELL_MARGIN);
		context.lineTo(x+cellSize*RIGHT_CELL_MARGIN, y+cellSize*RIGHT_CELL_MARGIN);
		context.stroke();
		context.beginPath();
		context.moveTo(x+cellSize*LEFT_CELL_MARGIN, y+cellSize * RIGHT_CELL_MARGIN);
		context.lineTo(x+cellSize*RIGHT_CELL_MARGIN, y+cellSize*LEFT_CELL_MARGIN);
		context.stroke();
	}

	function drawCircle(x,y){
		context.beginPath();
		context.strokeStyle = CIRCLE_COLOR;
		context.arc(x+cellSize/2, y+cellSize/2,  cellSize * DEFAULT_CELL_RADIUS , 0, 2 * Math.PI, true);
		context.stroke();
	}

	function changeTurn(){
		if (oldData == 1){
			data = 2;
		} else if (oldData == 2){
			data = 1;
		}
		return data;
	}
				
	function refresh(){
		context.clearRect(0, 0, canvasWidth, canvasHeight);
		drawGrid();
		for (var i=0; i<numberOfCells; i++){
	    	for (var j=0; j<numberOfCells; j++) {
	        	switch (map[j][i]) {
		       		case 1:
	               		drawСross(i*cellSize, j*cellSize);
	            		break;
		       		case 2:
		           		drawCircle(i*cellSize, j*cellSize);
		           		break;
		       	}
	      	}
		}
	}
	
    return {
    	getCurrentGame:function (){
	    	var thisGame = currentGame;
	    	return thisGame;
	    },

    	initGame: function(){
			gameToken = $location.search().token;
			currentGame = storage.getCurrentGame(gameToken);
			gameMode = 'normal';
	    	map = currentGame.map;
	    	winner = 0;
	    	oldData = currentGame.gameLastTurn;			
			data = changeTurn();
	    	
	    	myCanvas = document.getElementById("myCanvas12");
			context = myCanvas.getContext("2d");
			numberOfCells = currentGame.size;
			cellSize = canvasWidth/numberOfCells;
			lineVidth = cellSize*LINE_WIDTH;    
	    	context.lineWidth = lineVidth;
	
	    	if (currentGame.state == GAME_STATE_READY ){
	    		currentGame.state=GAME_STATE_PLAYING;
	    		appendCanvasClick();
	    	} else {
	    		gameMode = 'viewing';
	    		data = currentGame.gameLastTurn;
	    		viewing();
	    	}
	    	refresh();
	    },
    	getCurrentTurn: function (){
	    	var currentTurn = data;
	    	return currentTurn;
	    },
    	getGameMode:function (){
	    	var mode = gameMode;
	    	return mode;
	    }
	};
});
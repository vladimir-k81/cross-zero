angular.module('angularApp',['ngStorage', 'ngRoute'])
angular.module('angularApp')
.config(function ($routeProvider, $locationProvider){
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider.when("/index.html", {
        templateUrl: "/templates/lobby.html",
        controller  : 'defaultCtrl'
    });
                    
    $routeProvider.when("/game", {
        templateUrl: "/templates/game.html",
        controller  : 'gameCtrl'
    });
                    
    $routeProvider.otherwise({
        templateUrl: "/templates/lobby.html",
        controller  : 'defaultCtrl'
    });
})
angular.module('angularApp')
.controller('gameCtrl', function($scope, $interval, storage, drawService, searchWinner, $location){
    var REFRESH_INTERVAL = 1000;

    drawService.initGame();

    $scope.currentGame = drawService.getCurrentGame();
    $scope.currentTime = + new Date();
    $scope.gameDuration=$scope.currentTime - $scope.currentGame.gameBeginTime;
    $scope.owner = $scope.currentGame.owner;
    $scope.opponent = $scope.currentGame.opponent;
    $scope.winner = $scope.currentGame.gameResult;
    $scope.currentTurn = drawService.getCurrentTurn();
    $scope.gameMode=drawService.getGameMode();
    
    if ($scope.gameMode == "viewing" && $scope.currentTurn == 1){
        $("#owner").addClass("underlineView");
    } else if ($scope.gameMode == "viewing" && $scope.currentTurn == 2){
        $("#opponent").addClass("underlineView");
    }
    
    // angular.element(".underline").addClass("underlineView");
    $interval(function(){
        $scope.currentTime = + new Date();
        $scope.gameDuration=$scope.currentTime - $scope.currentGame.gameBeginTime;
        $scope.winner = $scope.currentGame.gameResult;
    },REFRESH_INTERVAL);

    $scope.getWinner = function(){
        $scope.winner = $scope.currentGame.gameResult;
        $scope.gameLastAction = $scope.currentGame.gameLastAction;
        $scope.currentTurn = drawService.getCurrentTurn();
    };
    
    $scope.goToLobby = function(){
        $("#modal").modal('hide');    
        $location.path("/index.html");
        storage.setGameInProcess(false);
    };
    
    $scope.surrender = function(){
        searchWinner.surrender($scope.currentTurn);
        $scope.winner = $scope.currentGame.gameResult;
    };

});
angular.module('angularApp')
.controller('defaultCtrl', function ($scope, storage, $location, $interval, $routeParams){

    $scope.games=storage.getGames();
    
    $interval(function(){
            $scope.games=storage.getGames();
    },2000);

    $scope.owner = "Player 1";
    $scope.opponent = "Player 2";
    $scope.currentView = 'gameList';
    $scope.select = {value:3};
    $scope.fieldSize = 3;
    $scope.countForWin = 3;

    $scope.gameTypes = [
        {gameType:'3 x 3', value:3},
        {gameType:'4 x 4, четыре в ряд', value:4},
        {gameType:'5 x 5, четыре в ряд', value:5},
        {gameType:'6 x 6, четыре в ряд', value:6},
        {gameType:'Custom field', value:'custom'},
        ];

    $scope.changeFieldSize = function(size){
        if (size == 'custom'){
            $scope.fieldSize = 6;
            $scope.countForWin = 4;
            
        } else {
            if (size == 3){
                $scope.fieldSize = size;
                $scope.countForWin = size;
            } else {
                $scope.fieldSize = size;
                $scope.countForWin = 4;
            }
        }
    };
    
    $scope.createNewGame = function(){
        var gameToken =
            storage.createNewGame($scope.owner, $scope.opponent, $scope.fieldSize, $scope.countForWin);

        storage.setGameInProcess(true);
        $scope.goToGame(gameToken);
    };
    
    $scope.goToGame = function(gameToken){
        $location.path("/game").search({token: gameToken});
    };
    
    $scope.clickHandler = function(item){
        storage.setGameInProcess(true);
        $scope.goToGame(item.gameToken);
    };
});
angular.module('angularApp')
.directive('canvasDraw', function(){
    return {
        template: "<canvas width='400' height='400' id='myCanvas12' class='center-block' ng-click='getWinner()'></canvas>",
    };
});
angular.module('angularApp')
.factory('drawService', function(storage, $interval, $location, searchWinner){

	var gameToken = $location.search().token;
	var currentGame = storage.getCurrentGame(gameToken);
	var gameMode = 'normal';
	    
	var CROSS_COLOR = "rgb(188, 188, 188)";
	var CIRCLE_COLOR = "rgb(43, 168, 218)";
	var GRID_COLOR = "rgb(254, 214, 50)"; 
    
    var GAME_STATE_READY = "ready";
    var GAME_STATE_PLAYING = "playing";
    var GAME_STATE_DONE = "done";
    
    var LINE_WIDTH = 0.05;
	var RIGHT_CELL_MARGIN = 0.8;
	var LEFT_CELL_MARGIN = 0.2;
	var DEFAULT_CELL_RADIUS = 1 / 3;

    var MODAL_ID = 'modal';
    var REFRESH_INTERVAL_IN_VIEW_MODE = 2000;

	var myCanvas = document.getElementById("myCanvas12");
	var context = myCanvas.getContext("2d");
	var numberOfCells = currentGame.size;
	var winner = 0;
	var canvasWidth = myCanvas.width;
	var canvasHeight = myCanvas.height;
	var cellSize = canvasWidth/numberOfCells;
	var lineVidth = cellSize*LINE_WIDTH;    
    context.lineWidth = lineVidth;
    
    var map = currentGame.map;
    
    var oldData = currentGame.gameLastTurn;			
	var data = changeTurn();

    function appendCanvasClick(){
    	
    	myCanvas.onclick = function(event) { 
			if (winner == 0){
				var x = (event.pageX - myCanvas.offsetLeft) / cellSize | 0;
				var y = (event.pageY - myCanvas.offsetTop)  / cellSize | 0;
				currentGame.gameLastAction= storage.setCurrentTime();
				if (map[y][x] == 0){
					map[y][x] = data;
					oldData = data;
					currentGame.gameLastTurn=data;
					currentGame.map = map;
					storage.saveGame(currentGame);
					storage.setGameInProcess(true);
					changeTurn();
				} 
			refresh();
			winner = searchWinner.search(currentGame);
			searchWinner.findDrawResult(currentGame);
			}
		};
    }
    
    function viewing(){
    	var refreshInterval = $interval(function(){
    		currentGame = storage.getCurrentGame(gameToken);
    		map = currentGame.map;
    		refresh();
    		if(currentGame.state == GAME_STATE_DONE){
    			$interval.cancel(refreshInterval);
    		}
    	},REFRESH_INTERVAL_IN_VIEW_MODE);
    }

    function drawGrid(){
        context.strokeStyle = GRID_COLOR;
		for (var i = 1; i < numberOfCells; i++) {
			context.moveTo(cellSize*i, 0);
			context.lineTo(cellSize*i, canvasHeight);
			context.stroke();
		}
		for (var j = 1; j < numberOfCells; j++){
			context.moveTo(0, cellSize*j);
			context.lineTo(canvasWidth, cellSize*j);
			context.stroke();
		}
    }

    function drawСross(x,y){
		context.beginPath();
		context.strokeStyle = CROSS_COLOR;
		context.moveTo(x+cellSize*LEFT_CELL_MARGIN, y+cellSize*LEFT_CELL_MARGIN);
		context.lineTo(x+cellSize*RIGHT_CELL_MARGIN, y+cellSize*RIGHT_CELL_MARGIN);
		context.stroke();
		context.beginPath();
		context.moveTo(x+cellSize*LEFT_CELL_MARGIN, y+cellSize * RIGHT_CELL_MARGIN);
		context.lineTo(x+cellSize*RIGHT_CELL_MARGIN, y+cellSize*LEFT_CELL_MARGIN);
		context.stroke();
	}

	function drawCircle(x,y){
		context.beginPath();
		context.strokeStyle = CIRCLE_COLOR;
		context.arc(x+cellSize/2, y+cellSize/2,  cellSize * DEFAULT_CELL_RADIUS , 0, 2 * Math.PI, true);
		context.stroke();
	}

	function changeTurn(){
		if (oldData == 1){
			data = 2;
		} else if (oldData == 2){
			data = 1;
		}
		return data;
	}
				
	function refresh(){
		context.clearRect(0, 0, canvasWidth, canvasHeight);
		drawGrid();
		for (var i=0; i<numberOfCells; i++){
	    	for (var j=0; j<numberOfCells; j++) {
	        	switch (map[j][i]) {
		       		case 1:
	               		drawСross(i*cellSize, j*cellSize);
	            		break;
		       		case 2:
		           		drawCircle(i*cellSize, j*cellSize);
		           		break;
		       	}
	      	}
		}
	}
	
    return {
    	getCurrentGame:function (){
	    	var thisGame = currentGame;
	    	return thisGame;
	    },

    	initGame: function(){
			gameToken = $location.search().token;
			currentGame = storage.getCurrentGame(gameToken);
			gameMode = 'normal';
	    	map = currentGame.map;
	    	winner = 0;
	    	oldData = currentGame.gameLastTurn;			
			data = changeTurn();
	    	
	    	myCanvas = document.getElementById("myCanvas12");
			context = myCanvas.getContext("2d");
			numberOfCells = currentGame.size;
			cellSize = canvasWidth/numberOfCells;
			lineVidth = cellSize*LINE_WIDTH;    
	    	context.lineWidth = lineVidth;
	
	    	if (currentGame.state == GAME_STATE_READY ){
	    		currentGame.state=GAME_STATE_PLAYING;
	    		appendCanvasClick();
	    	} else {
	    		gameMode = 'viewing';
	    		data = currentGame.gameLastTurn;
	    		viewing();
	    	}
	    	refresh();
	    },
    	getCurrentTurn: function (){
	    	var currentTurn = data;
	    	return currentTurn;
	    },
    	getGameMode:function (){
	    	var mode = gameMode;
	    	return mode;
	    }
	};
});
angular.module('angularApp')
.factory('storage', function($interval, $localStorage){
    
    var REFRESH_INTERVAL = 2000;
    var MAX_IDLE_TIME = 300000;

    var sync_disable = false;

    var currentTime = + new Date();
    var games = [];
        
    if (angular.isDefined($localStorage.games)) {
        games = $localStorage.games;
    } else {
        $localStorage.games = games;
    }
    
    function localStorageSync(){
        if ($localStorage.games.length != games.length){
            games = $localStorage.games;
        } 
    }

    $interval(function(){
        if (sync_disable != true){
            localStorageSync();
            currentTime = + new Date();
            incremGameDuration();
        }
    },REFRESH_INTERVAL);
    
    function incremGameDuration(){
        for (var i=0; i<$localStorage.games.length; i++){
            if ($localStorage.games.length == games.length){
                $localStorage.games[i].gameDuration = currentTime - $localStorage.games[i].gameBeginTime;
            }
        }
        clearOldGames();
    }
    
    function clearOldGames(){
        for(var i = $localStorage.games.length-1; i > -1; i--){
	        if ($localStorage.gameInProcess == true){
	            if (currentTime - $localStorage.games[i].gameLastAction>MAX_IDLE_TIME){
                    games[i].show = false;
                }
	        } else {
	            if (currentTime - $localStorage.games[i].gameLastAction>MAX_IDLE_TIME){
                    deleteGame(i);
                }
	        }
	    }
    }
    
    function deleteGame(item){
        $localStorage.games.splice(item,1);
    }
    
    
    function createMap(numberOfCells, value){
        var map=[];
			for (var i=0; i<numberOfCells; i++){
				map[i] = [];
				for (var j = 0; j<numberOfCells; j++){
					map[i][j] = value;
				}
			}
		return map;
	}

    function findGame(gameToken){
        var findGame;
        for (var i =0; i<$localStorage.games.length; i++){
                if (gameToken == $localStorage.games[i].gameToken){
                    findGame = i;
                }
            }
        return findGame; 
    }

    function setGameInProcess(sign){
        $localStorage.gameInProcess = sign;
    }

    function syncEnable(){
        sync_disable = false;
    }

    return {
        createNewGame: function (owner, opponent, fieldSize, countForWin){
            var gameToken =  + new Date();
            $localStorage.games.push({
                gameToken: gameToken,
                owner: owner,
                ownerWin:false,
                opponent: opponent,
                opponentWin:false,
                size: fieldSize,
                countForWin: countForWin,
                map: createMap(fieldSize, 0),
                gameBeginTime:currentTime,
                gameLastAction:currentTime,
                gameDuration:0,
                gameResult:"",
                state: "ready",
                acessToken:"768b762c8c28",
                gameLastTurn:2,
                show:true,
            });
            sync_disable = true;
            return gameToken;
        },
        setCurrentTime: function (){
            currentTime = + new Date();
            return currentTime;
        },
        endGame:function (){
            setGameInProcess(false);
            syncEnable();
        },
        getGames: function (){
            var gamesInStorage = $localStorage.games;
            return gamesInStorage;
        },
        getCurrentGame: function (gameToken){
            var gameIndex = findGame(gameToken);
            return $localStorage.games[gameIndex];
        },
        getCurrentGameIndex:  function (gameToken){
            var gameIndex = findGame(gameToken);
            return gameIndex;
        },
        setTurn: function (turn){
            $localStorage.turn = turn;
        },
        setGameInProcess: function (sign){
            $localStorage.gameInProcess = sign;
        },
        saveGame:function (savingGame){
            var index = findGame(savingGame.gameToken);
            $localStorage.games[index] = savingGame;
        }
    };
    
});
angular.module('angularApp')
.factory('searchWinner', function(storage){
    
    var MODAL_ID = 'modal';
    var GAME_STATE_DONE = "done";
    
    var increm = [
        [0, -1],
        [1, -1],
        [1, 0],
        [1, 1],
        [0, 1],
        [-1, 1],
        [-1, 0],
        [-1, -1]
        ];
    
    var currentGame = [];
    var map = [];
    var winner = 0;
    var countForWin = 3;
    
	function searchNeighbor(x, y){
	    for(var i=0; i<increm.length; i++){
		    var yi = y + increm[i][0];
			var xi = x + increm[i][1];
			if (yi >= 0 && xi >=0 && yi <= map.length-1 && xi <= map.length-1){

			    if (map[y][x] == map[yi][xi]){
			    	searchVector(yi, xi, i);
			    }
			} 
	    }
	}
    
    function searchVector(y, x, track){
		var count=2;
	    var yi = y;
	    var xi = x;
	    for (var i = 2; i < countForWin; i++){
	        yi = yi + increm[track][0];
    		xi = xi + increm[track][1];
    		if (yi >= 0 && xi >=0 && yi <= map.length-1 && xi <= map.length-1){
				if (map[y][x] == map[yi][xi]){
					count++;
					if (count == countForWin && map[y][x] != 0){
	    						
						winner = map[y][x];
						winnerProcessing(winner);
					}
				}
			}
	    }
	}
	
	function winnerProcessing(winner){
		if (winner == 2){
		    currentGame.gameResult = currentGame.opponent;
		    currentGame.opponentWin = true;
		    currentGame.state = GAME_STATE_DONE;
			$("#"+MODAL_ID).modal('show');
		} 
	    				
	    if (winner == 1){
	    	currentGame.gameResult = currentGame.owner;
	    	currentGame.ownerWin = true;
	    	currentGame.state = GAME_STATE_DONE;
			$("#"+MODAL_ID).modal('show');
		}
		storage.saveGame(currentGame);
		storage.endGame();
	}
    
    return {
        search: function (game){
			winner = 0;
			currentGame = game;
	        map = currentGame.map;
	        countForWin = currentGame.countForWin;
		    for(var i=0; i<map.length; i++){
		        for(var j=0; j< map.length; j++){
					        	
		        	if (currentGame.map[j][i] == 0){
		        		continue;
		        	}
					        	
			        if (winner != 0){
			          	break;
			        }
			      searchNeighbor(i,j);
		        }
		    }
		    return winner;
		},
		
        findDrawResult: function findDrawResult(currentGame){
	        if (winner==0){
	        	var emptyCells = 0;
	        	for(var i=0; i<map.length; i++){
	    	        for(var j=0; j< map.length; j++){
	    	        	if (map[j][i] == 0){
	    	        		emptyCells++;
	    	        	}
	    	        }
	    	    }
	    		if (emptyCells==0){
	    			currentGame.gameResult = "draw";
	    	    	currentGame.state = GAME_STATE_DONE;
	    	    	storage.saveGame(currentGame);
	    			$("#"+MODAL_ID).modal('show');
	    			storage.endGame();
	    		}
	        }
	    },
        surrender:function surrender(player){
	    	if(player==1){
	    		winnerProcessing(2);
	    	} else  if (player==2){
	    		winnerProcessing(1);
	    	}
	    }
    };
});
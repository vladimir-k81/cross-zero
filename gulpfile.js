var gulp = require('gulp');
var tools  = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('hello', function() {
  console.log('Hello Vova');
});

gulp.task("build-js", function () {
    return gulp.src([
        "scripts/**/*.js"
    ])
        .pipe(concat("core.js"))
        .pipe(gulp.dest("build"))
        .pipe(ngAnnotate({add: true}))
        .pipe(uglify())
        .pipe(rename("core.min.js"))
        .pipe(gulp.dest("build"))
        .on('error', tools.log);
});